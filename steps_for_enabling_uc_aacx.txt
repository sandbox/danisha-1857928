Steps for using the uc_aacx module.

1) In the ajax attribute calculations module (uc_aac.module)
there is a single list price on the edit product page,
which remains as it is on the product page,
while the sell price or the price changes with respect to the attribute.

2) After enabling the uc_aacx.module,
there is a extra option of list price on the options page per attibute.
Hence on the product page,
the list price as well as the sell price changes per attribute.









On test2

1) After enabling all the basic required ubercart and product modules.
2) before enabling any attributes calculation module uc_aac module,
the price wasnt changing with respect to the selected attribute.
It started changing when the uc_aac module was enabled
3) The list price has to be enabled from the backend in order to
see the product list price on the product page.
4) Just enabled the uc_aacx module and the list price
and the sell price are both reflecting on the product page.
